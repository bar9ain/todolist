0. TypeScript
TypeScript is a free and open source programming language developed and maintained by Microsoft. It is a strict superset of JavaScript, and adds optional static typing and class-based object-oriented programming to the language. TypeScript may be used to develop JavaScript applications for client-side or server-side (Node.js) execution.
TypeScript is designed for development of large applications and transcompiles to JavaScript. As TypeScript is a superset of JavaScript, any existing JavaScript programs are also valid TypeScript programs. 
A good introduction to the language can be found here: http://www.typescriptlang.org/Tutorial. 
Questions:
1.	What is Typescript? How is it different from javascript?
2.	List some features of Typescript?
3.	List some benefits of using Typescript?
4.	Who developed Typescript and what is the current stable version of Typescript?
5.	Tell the minimum requirements for installing Typescript. Also mention the steps involved in it.
6.	List the built-in types in Typescript.
7.	What are variables in Typescript? How to create a variable in Typescript?
8.	What do you mean by interfaces? Explain them with reference to Typescript.
9.	How to compile a Typescript file?
10.	What do you understand by classes in Typescript? List some features of classes.
11.	Is Native Javascript supports modules?
12.	How to compile multiple Typescript file into a single .js file?
13.	How to Call Base Class Constructor from Child Class in TypeScript?
14.	What are Closures in Javascript?
15.	List types of scopes available in Javascript?
16.	What are Modules in Typescript?
17.	What is namespace in Typescript? How to declare a namespace in Typescript?
18.	Explain Decorators in Typescript? List type of Decorators available in Typescript?
19.	What are Mixins? Why we use it?
20.	What is default visibility for properties/methods in Typescript classes?

1. The Core Concepts of Angular
https://vsavkin.com/the-core-concepts-of-angular-2-c3d6cbe04d04

2. Getting started
The codebase project is built base on Angular 2+ tour of Heroes example.
Download and extract this zip file: https://angular.io/generated/zips/toh-pt6/toh-pt6.zip
Install node_modules, run: npm install
Start application, run: npm start
Application will automatically start in local default browser

3. Data binding
Read this article: https://angular.io/docs/ts/latest/guide/template-syntax.html#!#binding-syntax
Questions:
1.	Why is data binding needed?
2.	What is one-way/two-way binding?
3.	Compare between one-way/two-way binding and define when it should be used.

4. Template Syntax
Implement project of previous step as described in:
https://angular.io/docs/ts/latest/tutorial/toh-pt2.html
Target: Understand Angular 2+ template syntax (ngFor, ngIf…) and use it to render UI.
Questions:
1.	Compare *NgIf and CSS style display: none , visibility: hidden. Why should we use *NgIf?
2.	How could we add event listener for an UI element? E.g: add oninput listener for an input field.
Ref: https://angular.io/docs/ts/latest/guide/template-syntax.htm

5. Component
Implement project of previous step as describe in:
https://angular.io/docs/ts/latest/tutorial/toh-pt3.html
Target: Understand how to define a component in Angular 2+ and integrate it into project.
Questions:
1.	What is Angular 2+ component?
2.	When should component be defined and why?
Ref: https://angular.io/docs/ts/latest/guide/cheatsheet.html

6. Injection
Implement project of previous step as describe in:
https://angular.io/docs/ts/latest/tutorial/toh-pt4.html
Target: Understand how to define a  @Injectable  Service in Angular 2+ and its scope.
Questions:
1.	Why dependency injection?
2.	What happen if services is not defined as @Injectable?
3.	Scope of @Injectable object?
4.	Open question: Could we have any way to get @Injectable object at runtime without define it as component constructor parameter?
Ref: https://angular.io/docs/ts/latest/guide/cheatsheet.html

7. HTTP
Read this article: https://angular.io/docs/ts/latest/api/http/index/Http-class.htm
Questions:
1.	Why should global http handler be needed?
2.	Why should global http handler be overrided?

8. Pipe
Read this article: https://angular.io/docs/ts/latest/guide/pipes.html
Questions:
1.	Compare pure pipes, impure pipes and impure async pipes?
2.	What type of above pipes?

9. Reactive Form validation
Read this article: https://angular.io/docs/ts/latest/cookbook/form-validation.html#!#reactive
Questions:
1.	Compare reactive form validation with template-driven forms validation.
2.	What is difference between FormControl.touched, FormControl.untouched, FormControl.pristine, FormControl.dirty.
3.	How could we disabled an ui element with form control? 

10. Learning @Input, @Output, Event Emitter
Read this article:  https://angular.io/docs/ts/latest/cookbook/ts-to-js.html#!#sts=Input%20and%20Output%20Metadata

Question :
1.	What is @Input, @Output and EventEmitter ? 
2.	When to use it ?
3.	What is EventEmitter used for ?
4.	Compare EventEmitter and rxjs Observables and define when to use them ?

11. Directives
Angular 2+ have 3 types of directive:
•	Component directives, it is <template> in Angular 2+
•	Attribute directives changes the appearance or behavior of an element
•	Structural directives changes the DOM layout by adding and removing DOM elements
Write a attribute directive that will display male hero name as blue  color and female hero name as red color in heroes list screen.
Questions:
1.	When do we need to define a directive?
Ref: https://angular.io/docs/ts/latest/guide/attribute-directives.html
https://angular.io/docs/ts/latest/guide/structural-directives.html