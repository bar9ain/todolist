<?php

class BaseController
{
    private $controller;
    private $params;

    public function __construct($params = null)
    {
        $this->parseURL();
        $this->load();
    }

    private function parseURL()
    {
        $url = isset($_GET["url"]) ? $_GET["url"] : null;
        if ($url !== null) {
            $urlArray = explode("/", $url);
            $this->controller = $urlArray[0];
            array_shift($urlArray);
            if (count($urlArray) > 0) {
                $this->params = array();
                foreach ($urlArray as $element) {
                    if (trim($element) !== null) {
                        array_push($this->params, $element);
                    }
                }
            }
        }
    }

    private function load()
    {
        $controllerName = ucfirst($this->controller) . "Controller";
        $path = "controllers/" . $this->controller . ".php";
        if (file_exists($path)) {
            include $path;
            new $controllerName($this->params);
        }
        else {
            include "controllers/HomeController.php";
            new HomeController($this->params);
        }
    }
}