<?php

class HomeController extends Controller
{
    private $params = array();

    public function __construct($params = array())
    {
        parent::__construct();
        include "models/TodoModel.php";
        $this->params = $params;
        $this->index();
    }

    public function index()
    {
        $todoModel = new TodoModel();
        $action = null;

        if (count($this->params) > 1) {
            $action = trim($this->params[0]);
            $id = trim($this->params[1]);
        } elseif (count($this->params) > 0) {
            $action = trim($this->params[0]);
        }

        switch ($action) {
            case "delete":
                $todoModel->deleteTask($id);
                header("Location:" . PATH . "/home");
                break;


            case "task":
                if (isset($_POST['submit'])) {
                    $data = array();
                    $data["id"] = $id;
                    $data["work_name"] = isset($_POST['work_name']) ? $_POST['work_name'] : NULL;
                    $data["starting_date"] = isset($_POST['starting_date']) ? $_POST['starting_date'] : date("Y-m-d");
                    $data["ending_date"] = isset($_POST['ending_date']) ? $_POST['ending_date'] : date("Y-m-d");
                    $data["status"] = isset($_POST['status']) ? $_POST['status'] : 1;

                    if (isset($_POST["action"]) && $_POST["action"] === "edit")
                        $todoModel->updateTask($data);
                    else
                        $todoModel->addTask($data);

                    header("Location:" . PATH . "/home");
                    return;
                }

                $this->view->title = "Add Task";
                if (is_numeric($id)) {
                    $this->view->model = $todoModel->getTask($id);
                    if ($this->view->model != null) {
                        $this->view->action = "edit";
                        $this->view->title = "Update Task";
                    }
                }
                include "models/StatusModel.php";
                $statusModel = new StatusModel();
                $this->view->statusList = $statusModel->getList();
                $this->view->render("home/editor");
                break;


            case "calendar":
                $this->view->todoList = $todoModel->getTodoList();
                $this->view->title = "Calendar";
                $this->view->render("home/calendar");
                break;


            default:
                $this->view->todoList = $todoModel->getTodoList();
                $this->view->title = "Todo List";
                $this->view->render("home/index");
                break;
        }
    }
}