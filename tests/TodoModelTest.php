<?php

class TodoModelTest extends PHPUnit\Framework\TestCase
{
    public function testListIsEmpty()
    {
        $model = new TodoModel();
        $this->assertEmpty($model->getTodoList());
    }

    public function testListCountIsCorrect()
    {
        $model = new TodoModel();
        $this->assertEquals(1, count($model->getTodoList()));
    }

    public function testGetTaskFromDb() {
        $model = new TodoModel();
        $item = $model->getTask(7);
        $this->assertNotNull($item);
    }
}
