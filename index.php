<?php

session_start();
include "config/config.php";
include "core/Database.php";
include "core/Controller.php";
include "core/Model.php";
include "core/View.php";
include "core/Utils.php";
include "controllers/BaseController.php";

new BaseController();