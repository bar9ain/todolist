<?php

class View
{
    public function __construct()
    {
    }

    public function render($path, $include = true)
    {
        if ($include) {
            include "views/header.php";
            include "views/$path.php";
            include "views/footer.php";
        } else {
            include "views/$path.php";
        }
    }
}
