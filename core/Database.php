<?php

class Database
{
    protected $conn;

    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        global $host;
        global $username;
        global $password;
        global $database;

        $this->conn = mysqli_connect($host, $username, $password, $database);
        if ($this->conn) {
            mysqli_set_charset($this->conn, "UTF8");
        }
    }

    public function read($sql)
    {
        $result = array();
        $this->connect();
        $query = mysqli_query($this->conn, $sql);
        while (($row = mysqli_fetch_object($query)) != null) {
            $result[] = $row;
        }
        if (mysqli_errno($this->conn) > 0)
            echo mysqli_error($this->conn);
        $this->disconnect();
        return $result;
    }

    public function readOne($sql) {
        $query = mysqli_query($this->conn, $sql);
        $row = mysqli_fetch_object($query);
        if (mysqli_errno($this->conn) > 0)
            echo mysqli_error($this->conn);
        $this->disconnect();
        return $row;
    }

    public function exec($sql) {
        mysqli_query($this->conn, $sql);
        if (mysqli_errno($this->conn) > 0)
            echo mysqli_error($this->conn);
        $this->disconnect();
    }

    public function disconnect()
    {
        $this->conn = null;
    }

    public function getConnection() {
        return $this->conn;
    }
}
