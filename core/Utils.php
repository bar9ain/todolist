<?php

class Utils
{
    public static function renderSelect(array $data, $name, $value, $text, $cssClass, $required = false, $selectedValue = null)
    {
        $req = $required ? "required" : "";
        echo "<select name=\"$name\" class=\"$cssClass\" $req>";
        foreach ($data as $item) {
            $val = $item->$value;
            $tex = $item->$text;
            $selected = ($val === $selectedValue ? "selected" : "");
            echo "<option value=\"$val\" $selected>$tex</option>";
        }
        echo "</select>";
    }
}