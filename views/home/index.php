<div class="wrapper">
    <h2>Todo List</h2>
    <div class="controls">
        <a href="<?= PATH ?>/home/task/new" class="button">Add Task</a>
        <a href="<?= PATH ?>/home/calendar" class="button">Calendar</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="text-align: center">Num</th>
            <th>Work name</th>
            <th>Starting date</th>
            <th>Ending date</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0;
        foreach ($this->todoList as $item): ?>
            <tr>
                <td class="auto"><?= ++$i ?></td>
                <td data-title="Work name"><?= $item->work_name ?></td>
                <td data-title="Starting date"><?= date("d-m-Y", strtotime($item->starting_date)) ?></td>
                <td data-title="Ending date"><?= date("d-m-Y", strtotime($item->ending_date)) ?></td>
                <td data-title="Status"><?= $item->status_name ?></td>
                <td data-title="Actions">
                    <a href="<?= PATH ?>/home/task/<?= $item->id ?>">Edit</a>
                    <a href="<?= PATH ?>/home/delete/<?= $item->id ?>" class="delete">Delete</a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
