<div class="calendar-wrapper">
    <div class="calendar-note">
        <a href="<?= PATH ?>/home"><h2>TodoList</h2></a>
        <table class="table">
            <thead>
            <tr>
                <th width="60">Colour</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="colour-square planning"></div>
                </td>
                <td>Planning</td>
            </tr>
            <tr>
                <td>
                    <div class="colour-square doing"></div>
                </td>
                <td>Doing</td>
            </tr>
            <tr>
                <td>
                    <div class="colour-square complete"></div>
                </td>
                <td>Complete</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="calendar"></div>
</div>

<script src="<?= PATH ?>/assets/js/moment.min.js"></script>
<script src="<?= PATH ?>/assets/js/fullcalendar.min.js"></script>

<script>

    $(document).ready(function () {
        colours = ["#00c0ef", "#0073b7", "#00a65a"];
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: '<?= date("Y-m-d") ?>',
            navLinks: true,
            editable: true,
            eventLimit: true,
            events: [
                <?php foreach ($this->todoList as $task): ?>
                {
                    id: '<?= $task->id ?>',
                    title: '<?= $task->work_name ?>',
                    start: '<?= $task->starting_date ?>',
                    end: '<?= $task->ending_date ?>',
                    backgroundColor: colours[<?= $task->status_id - 1 ?>],
                    borderColor: colours[<?= $task->status_id - 1 ?>]
                },
                <?php endforeach; ?>
            ]
        });
    });

</script>