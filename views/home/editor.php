<div class="editor-wrapper">
    <h2><?= $this->title ?></h2>
    <form method="post">
        <input type="hidden" name="action" value="<?= isset($this->action) ? $this->action : null ?>">

        <label>Work name</label>
        <input type="text" name="work_name" placeholder="Work name" class="input"
               value="<?= isset($this->model) ? $this->model->work_name : null ?>" required>

        <label>Starting date</label>
        <input type="date" name="starting_date" placeholder="Starting date" class="input"
               value="<?= isset($this->model) ? $this->model->starting_date : null ?>" required>

        <label>Ending date</label>
        <input type="date" name="ending_date" placeholder="Ending date" class="input"
               value="<?= isset($this->model) ? $this->model->ending_date : null ?>" required>

        <label>Status</label>
        <?php Utils::renderSelect($this->statusList, "status", "status_id", "status_name",
            "input", true, isset($this->model) ? $this->model->status : null) ?>
        <div class="controls">
            <a href="<?= PATH ?>/home" class="button">Back</a>
            <button name="submit" type="submit" class="button">Submit</button>
        </div>
    </form>
</div>