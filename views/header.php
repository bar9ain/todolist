<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,500,700">
    <link rel="stylesheet" href="<?= PATH ?>/assets/css/style.css">
    <link rel="stylesheet" href="<?= PATH ?>/assets/css/fullcalendar.min.css">
    <link rel="stylesheet" href="<?= PATH ?>/assets/css/fullcalendar.print.min.css" media="print">

    <script src="<?= PATH ?>/assets/js/jquery.min.js"></script>

    <title><?= $this->title ?></title>
</head>
<body>
