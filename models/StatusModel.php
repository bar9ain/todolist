<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30/7/2018
 * Time: 8:14 PM
 */

class StatusModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList()
    {
        $sql = "SELECT status_id, status_name
                FROM task_status";
        $query = $this->db->read($sql);
        return $query;
    }
}