<?php

class TodoModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTodoList()
    {
        $sql = "SELECT todo.id, todo.work_name, todo.starting_date, todo.ending_date, task_status.status_id, task_status.status_name
                FROM todo 
                INNER JOIN task_status ON todo.status = task_status.status_id";
        $query = $this->db->read($sql);
        return $query;
    }

    public function getTask($id)
    {
        $id = mysqli_real_escape_string($this->db->getConnection(), $id);
        $sql = "SELECT todo.id, todo.work_name, todo.starting_date, todo.ending_date, todo.status
                FROM todo
                WHERE todo.id = $id";
        $task = $this->db->readOne($sql);
        return $task;
    }

    public function addTask($data)
    {
        extract($data);
        $work_name = mysqli_real_escape_string($this->db->getConnection(), $work_name);
        $starting_date = mysqli_real_escape_string($this->db->getConnection(), $starting_date);
        $ending_date = mysqli_real_escape_string($this->db->getConnection(), $ending_date);
        $status = mysqli_real_escape_string($this->db->getConnection(), $status);

        $sql = "INSERT INTO todo(work_name, starting_date, ending_date, status)
                VALUES ('$work_name', '$starting_date', '$ending_date', '$status')";
        $this->db->exec($sql);
    }

    public function updateTask($data)
    {
        extract($data);
        $work_name = mysqli_real_escape_string($this->db->getConnection(), $work_name);
        $starting_date = mysqli_real_escape_string($this->db->getConnection(), $starting_date);
        $ending_date = mysqli_real_escape_string($this->db->getConnection(), $ending_date);
        $status = mysqli_real_escape_string($this->db->getConnection(), $status);

        $sql = "UPDATE todo SET
                        work_name='$work_name',
                        starting_date='$starting_date',
                        ending_date='$ending_date',
                        status='$status'
                WHERE id = $id";
        $this->db->exec($sql);
    }

    public function deleteTask($id)
    {
        $id = mysqli_real_escape_string($this->db->getConnection(), $id);
        $sql = "DELETE FROM todo WHERE id = $id";
        $this->db->exec($sql);
    }
}